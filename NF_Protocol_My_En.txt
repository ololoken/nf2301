;antonio-dj 4pda.ru/forum
COM2 115200
-----------------------------------------------------------------------------------------------
First byte - command
Second byte - data lenght
Next bytes - data
-----------------------------------------------------------------------------------------------
Commands:
11 - Reset?
12 - Get module info
13 - "GetLocalDeviceStatus"
14 - AT command ???
16 - Write name of device
17 - Set visibility
18 - Off ???
1C - Find devices "InquiryDevice"
1E - Request name of device
20 - Write PIN nF2301
21 - require a PIN
22 - Request to create a pair of "PairRemoteDevice" ?
23 - Request paired devices in memory
24 - Clear paired devices in memory
25 - Establish SLC Paired Device (SLC -Service Level Connection)
26 - Disconnect Paired Device
27 - ???
28 - ???
29 - "Answer" Receive a call
2A - "Reject"
2B - "Terminate"
2C - Dial number
2E - ReDial?
2F - VoiceDial?
30 - Select sound
31 - DTMF
32 - SetVolume (Not supported 82)
33 - MicVol (Not supported 82)
34 - ??? (Not supported 82)
35 - "EnhancedCallCtrl(int)"
38 - Redial?
3D - AVRCP Commands
3E - "CallHistoryDownload"
45 - "SendSMS"
4A - Transfer data ?
4E - "NFTest"?
4F - Mute
53 - "SetRingToneLevel" 
54 - Request to contacts "GetPhoneBookDownLoad"?
57 - "VRState" (Not supported 82)
59 - "SetMicInput" (Not supported 82)
5A - "SetMicGain" (Not supported 82)
62 - "Set_3g_mode"
7F - "SandBoxCmd"
Statuses:
80 - Start init?
81 - Finish init?
82 - Info about device to paired (after 22...)
83 - Connection result?
84 - Call state
85 - State of sound destination?
89 - Detected incoming phone number
8D - BT signal power
8E - ?
8F - ?
90 - ?
93 - ???  Detected phone number
95 - State of A2DP ?
96 - Sound state or mic mute state ?
97 - State of BT connection
AC - Info about device to paired ?
B9 - End list of paired devices in memory
BB - Disconnecting BT ?
BE - ?
C6 - After start init, BUSY?
C7 - Out MUTE state ?
D1 - ?
D2 - ?
Errors:
81 - Command not perform
82 - Unknown command
83 - Uncorrect data or ?
89 - No Data (Command 23)
8B - 
8E - 
91 - 
92 - already running ?
-----------------------------------------------------------------------------------------------
Tx: 11 01 01 - "PowerOn" Reset or init
After reset messages
Rx: C7 01 01 - Out MUTE state ?
Rx: 80 00 - Start init ?
Rx: C6 02 01 00 - State of ....?
Rx: 95 02 01 00 - State of A2DP ?
Rx: 97 02 01 00 - State of BT connection
Rx: 84 03 01 00 00 - State of call ?
Rx: 81 00 -  Finish init ?

Tx: 12 00 - Request information about MMC "GetFWVersion"
Rx: 12 1D 00 00 17 53 44 43 C5 00 7A 00 09 4B 6F 27 11 04 30 30 30 30 08 4D 4D 43 20 32 31 39 30 
          xx : Return value 00 = Ok
             00 17 53 44 43 C5 : MAC NF2301
                               00 7A 00 09 : CoD (Class of Device) http://bluetooth-pentest.narod.ru/software/bluetooth_class_of_device-service_generator.html   and   http://www.ampedrftech.com/guides/cod_definition.pdf
                                  7A - Classes: Telephony (Cordless telephony, Modem, Headset service, ...), 
                                                Audio (Speaker, Microphone, Headset service, ...), 
                                                Object Transfer (v-Inbox, v-Folder, ...), 
                                                Capturing (Scanner, Microphone, ...), 
                                                Networking (LAN, Ad hoc, ...)
                                     00 - Device Class: Miscellaneous
                                        09 - Hands-free Device
                                           4B 6F 27 11 : ?FW version? Core version? ("Ko'
                                                       04 : number of bytes PIN
                                                          30 30 30 30 : PIN
                                                                      08 : number of bytes of name
                                                                         4D 4D 43 20 32 31 39 30 : name (MMC 2190)

Tx: 13 00 - Request "GetLocalDeviceStatus"
Rx: 13 0D 00 02 03 00 00 0C 0C 01 01 01 01 01 00 
          00 : Return value 00 = Ok
             02 : Power//Status 1 ?
                03 : Visibility ?
                   00 : Status 3 Active call Sound Out to MMC (85)?
                      00 : Status 4 ?
                         0C : Volume (SetVolume 32)
                            0C : Status 6 volume?
                               01 : Call (HFP) State (84)
                                  01 : A2DP Status (95)
                                     01 : AVRCP State (97)
                                        01 : Status 10 ?
                                           01 : Status 11 ?
                                              00 : Status 12 ?

Tx: 16 08 4D 4D 43 20 32 31 39 30 - Write name of device
       08 : number of bytes
          4D 4D 43 20 32 31 39 30 : name of device (MMC 2190)
Rx: 16 09 00 4D 4D 43 20 32 31 39 30 - Write name of device Ok
          00 : Return value 00 = Ok
             4D 4D 43 20 32 31 39 30 : name of device (MMC 2190)

Tx: 17 01 0x - Set visibility
          00 : Invisible
		      02 : Only paired?
		      03 : visible to everyone?
Rx: 17 02 00 0x 
          xx : Return value 00 = Ok

Tx: 1C 03 00 1C 0A - Find devices "InquiryDevice"
             xx xx - CoD filter or timeout/maxdevice settings?
    1C 03 01 05 06 - Abort inquiry
Rx: 1C 0B 00 7C FA DF CA D9 E8 00 7A 02 0C - Device finded
          00 : Return value 00 = Ok
             7C FA DF CA D9 E8 : MAC
                               00 7A 02 0C : CoD (Class of Device)
Rx: 1C 0B 01 00 00 00 00 00 00 00 00 00 00 - Not found

Tx: 1E 06 7C FA DF CA D9 E8 - Request name of device
          7C FA DF CA D9 E8 : MAC
Rx: 1E 16 00 7C FA DF CA D9 E8 0E 00 4C 00 6F 00 62 00 73 00 74 00 65 00 72 - Response
          00 : Return value 00 = Ok
             7C FA DF CA D9 E8 : MAC
                               0E : number of bytes of name
                                  00 4C 00 6F 00 62 00 73 00 74 00 65 00 72 : name unicode UTF16-BE (Lobster)

Tx: 20 04 30 30 30 30 - Write PIN nF2301
       04 : number of bytes
          30 30 30 30 : PIN  ASCII
Rx: 20 05 00 30 30 30 30 
       05 : number of bytes
          00 : Return value 00 = Ok
             30 30 30 30 : PIN

Tx: 21 01 01 - require a PIN
Rx: 21 01 00

Tx: 22 06 E6 68 46 AE C2 35 - Request to create a pair of "PairRemoteDevice" ?
          E6 68 46 AE C2 35 : MAC
Rx: 22 01 00 - Request Ок

Tx: 23 00 - Request paired devices in memory
Rx: 23 1D 00 01 E6 68 46 AE C2 35 12 00 46 00 6C 00 79 00 20 00 49 00 51 00 34 00 34 00 31 00 00
          00 : Return value 00 = Ok
             01 :  ? device index
                E6 68 46 AE C2 35 : MAC 
                                  12 : number of bytes of name (18)
                                     00 46 00 6C 00 79 00 20 00 49 00 51 00 34 00 34 00 31 : name unicode (Fly IQ441)
                                                                                           00 00 : ???
Rx: 23 01 89 - No devices in memory
Rx: B9 00 - End list of devices

Tx: 24 01 xx - Clear all paired devices in memory?
          xx - dev index
          FF - all
Rx: 24 02 00 01 
          00 : Return value 00 = Ok
             01 : paired dev index

Tx: 25 02 0x 0x - connect
           x : auto connect on/off
              x : paired device index
Tx: 25 02 01 01 - Request to BT auto connection ?
Tx: 25 07 01 7C FA DF CA D9 E8 - "EstablishSLCPairedDeviceAt(MAC)" (SLC -Service Level Connection)
          01 : On (00 - Off autoconnect) ???
             7C FA DF CA D9 E8 : MAC
Rx: 25 01 xx - Response
          00 : Return value 00 = Ok
          83 : Error

Tx: 25 02 01 09 - connect dev SLC//Auto connection 
Rx: 25 02 xx 09 - Response
          00 : Return value 00 = Ok
          92 : Error

Tx: 26 01 xx - Disconnect paired device//"PowerOff" 2-nd
          01 : ?index
          FF : Disconnect all
Rx: 26 02 91 FF - Response
          00 : Return value 00 = Ok
          91 : Error
             FF : device index (in this case all devices)

Tx: 29 00 - "Answer" Receive a call
Rx: 29 01 xx - Response
          00 : Return value 00 = Ok
          83 : Error

Tx: 2A 00 - Terminate 
Rx: 2A 01 xx - Response
          00 : Return value 00 = Ok
          83 : Error

Tx: 2B 00 - Reject 

Tx: 2C 06 32 30 31 35 31 39 - Dialing 
       06 : number of bytes
          32 30 31 35 31 39 : Number (201519)
Rx: 2C 01 xx - Response
          00 : Return value 00 = Ok
          83 : Error

Tx: 2E 00 - ReDial?

Tx: 2F 00 - VoiceDial?

Tx: 30 01 xx - Select sound
          00 : To phone
          01 : To MMC
Rx: 30 01 xx - Response
          00 : Return value 00 = Ok
          83 : Error

Tx: 31 01 xx - DTMF

Tx: 32 01 xx - SetVolume
Rx: 32 03 00 xx 09 - Actual Volume


Tx: 33 01 xx - MicVol (xx = 0 -> Mute)
Rx: 33 01 82
          82 : Error Unknown command

Rx: 34 01 00 - ?

Tx: 35 02 04 FF - "EnhancedCallCtrl(int)"
          xx : command

Tx: 38 00 - Redial?
Rx: 38 01 00 - Response
          00 : Return value 00 = Ok

Tx: 3D 01 xx - AVRCP Commands
          00 : Stop ?
          01 : Play
          02 : Pause
          03 : AudioTransfer
          04 : Backward
          05 : Forward
          11 : SendCmd_GetAvrcpSongInfo
Rx: 3D 02 00 01 - Response Ок?
          00 : Return value 00 = Ok

Tx: 3E 03 xx xx 00 - PBAP //"CallHistoryDownload" 
          01 // - callhistory
             xx //?subtype CH: 04 - Rec, 05 - dil
          0D 03 00 //call history all
          05 01 00 //stop call log
          0C 02 00 //down phonebook
          06 01 00 //phonebook 2 ??
          06 00 00 //phonebook abort obex disconnect
          FF 01 00 //download phonebook using AT commands storage

Tx: 45 1B 00 0D 30 30 31 31 30 30 30 39 39 31 32 31 34 33 36 35 38 37 46 39 30 30 30 38 41 37 30 30 1A {E...00110009921436587F90008A700.} - "SendSMS" ???
             0D : PDU size (number of words + 3 bytes)
                30 30 : SCA (ASCII)
                      31 31 : PDU type (ASCII)
                            30 30 : MR (ASCII)
                                  30 39 : Len of number
                                        39 31 : Type 81 - national, 91 - international
                                              32 31 34 33 36 35 38 37 46 39 : Call number 123456789F (F - finish)
                                                                            30 30 : PID
                                                                                  30 38 : DCS (00 = 7 bit, 08 = UCS2)
                                                                                        41 37 : Validity-Period A7=167=24hours
                                                                                              30 30 : Data-Length (symbols)
                                                                                                    1A : end of file

Tx: 4A nn dd dd dd - Transfer data ?

Tx: 4E 02 01 DC - "NFTest"

Tx: 4F 01 xx - Microphone Mute
          00 : //mute
          01 : //unmute
Rx: 4F  - ?

Tx: 52 0A E6 68 46 AE C2 35 00 5A 02 04 - ? Request to connection??
          E6 68 46 AE C2 35 : MAC
                            00 5A 02 04 : CoD (Class of Device)
Rx: 52 01 82 - Response
          00 : Return value 00 = Ok
          82 : Error

Tx: 53 02 xx 00 - "SetRingToneLevel"

Tx: 54 01 03 - OBEX: Request to contacts "GetPhoneBookDownLoad"?
          03 - action type
Tx: 54 A6 01 00 9F 00 00 00 9F 02 00 00 6A 1D 2D 2F 2F 53 59 4E 43 4D 4C 2F 2F 44 54 44 20 53 79 6E 63 4D 4C 20 31 2E 31 2F 2F 45 4E 6D 6C 71 03 31 2E 31 00 01 72 03 53 79 6E 63 4D 4C 2F 31 2E 31 00 01 65 03 30 00 01 5B 03 31 00 01 6E 57 03 2F 00 01 01 67 57 03 50 43 20 53 75 69 74 65 00 01 01 5A 00 01 4C 03 34 30 30 30 00 01 01 01 00 00 6B 46 4B 03 30 00 01 4F 03 32 30 36 00 01 54 67 57 03 43 6F 6E 74 61 63 74 73 00 01 01 5A 00 01 57 03 43 6F 6E 74 61 63 74 73 00 01 01 5A 00 01 53 03 74 65 78 74 2F 78 2D 76 63 61 72 64 00 01 01 01 01 00 00 12 01 01

Tx: 57 01 xx - "VRState"

Tx: 59 01 04 - "SetMicInput"
          xx : Input

Tx: 5A 01 04 - "SetMicGain"
          xx : Gain
          
Tx: 7F 02 xx 00 - "SandBoxCmd"
          xx : cmd

Rx: 80 00 Start init

Rx: 81 00 Finish init

Rx: 82 15 7C FA DF CA D9 E8 0E 00 4C 00 6F 00 62 00 73 00 74 00 65 00 72 - Info about device to paired (after 22...)
          7C FA DF CA D9 E8 : MAC
                            0E : number of bytes of name
                               00 4C 00 6F 00 62 00 73 00 74 00 65 00 72 : name unicode (Lobster)

Rx: 83 08 00 01 E6 68 46 AE C2 35 - Pair result?
          00 : Return value 00 = Ok
          02 : Error
             01 : paired device index
                E6 68 46 AE C2 35 : MAC

Rx: 84 nn xx - Call state
Rx: 84 03 01 00 00 - HFP ready(to be connected)
Rx: 84 03 02 00 00 - HFP connecting
Rx: 84 03 03 00 00 - HFP connected / Finish (or no) call
          03 :  Finish (or no) call
Rx: 84 03 04 01 00 - Outgoing calls without specifying a number 
Rx: 84 09 04 01 06 32 30 31 35 31 39 - Outgoing calls
          04 : Outgoing calls
             01 : device index//State?
                06 : number of bytes
                   32 30 31 35 31 39 : Number (201519)
Rx: 84 03 05 01 00 - Incoming call
          05 : Incoming call
             01 : device index //State?
                00 : number of bytes
Rx: 84 03 06 01 00 - Active call
Rx: 84 09 06 01 06 32 30 31 35 31 39 - Active call
          06 : Active call
             01 : call/device index//State?
                06 : number of bytes
                   32 30 31 35 31 39 : Number (201519)
          08 : another call on hold

Rx: 85 02 00 01 - State of sound destination?
          00 : Sound to phone ?
          01 : Sound to MMC

Rx: 89 0E 01 0C 2B 37 31 32 33 34 35 36 37 38 39 30 - Detected phone number
          01 : call index
             0C : number of bytes (12)
                2B 37 31 32 33 34 35 36 37 38 39 30 : Number (+71234567890)

Rx: 8D 02 xx 01 - HFP (HandsFree Profile) BT signal power 00..05

Rx: 8E 02 00 0A - HFP (HandsFree Profile) Roaming?

Rx: 8F 02 xx 0A - HFP (HandsFree Profile) Battery charge

Rx: 90 02 01 01 - HFP (HandsFree Profile) HF Status?

Rx: 93 0E 01 00 02 00 00 00 07 38 37 35 38 34 31 35 - ???  Detected phone number
          01: call index
             00 02 00 00 00 : hfp state/volume/mic in/out /etc?
                            07 : number of bytes
                               38 37 35 38 34 31 35 : Number

Rx: 95 02 xx 00 - State of A2DP Connection?
          01 : A2DP ready(to be connected)
          02 : A2DP request connect
          03 : A2DP connecting
          04 : A2DP connected
          05 : A2DP streaming

Rx: 96 01 xx - Sound steream state? AG (audio gate) state?
          00 : no sound
          01 : steream sound (Music or system sounds)

Rx: 97 02 01 00 - State of BT connection AVRCP ???
          01 00 : ready(to be connected)
          02 01 : connecting
          03 01 : connected

Rx: AC 1B 02 01 E6 68 46 AE C2 35 12 00 46 00 6C 00 79 00 20 00 49 00 51 00 34 00 34 00 31
          02 : BT connecting state? profiles bits (0 bit - HPF, 1 bit - A2DP)?
             01 : paired device index// State?
                E6 68 46 AE C2 35 : MAC
                                  12 : number of bytes (18)
                                     00 46 00 6C 00 79 00 20 00 49 00 51 00 34 00 34 00 31 : name unicode (Fly IQ441)

Rx: B9 00 - End list of devices (See Tx: 23 00)

Rx: BB 02 01 01 - Disconnect event ?
          01 : Disconnect HFP
          02 : Disconnect A2DP

Rx: BE 02 02 01 - ??? Sending after: BB 02 02 01

Rx: C6 02 xx 0y - ? After start init, BUSY?
          01
          02
          03

Rx: C7 01 xx - Out MUTE state ?
          00 : Mute off
          01 : Mute on

Rx: D1 01 01 - ?

Rx: D2 02 00 63 - ???
          00 :
          03 :
             xx : ? 63, 67
          01 ef - ???


From "ABBTM BLUEAUDIO USER GUIDE"

A2DP: A2DP status will be reported to host upon changed: (95? command -1)
0  ready(to be connected)
1  connecting 
2  connected 
3  streaming 

AVRCP: AVRCP status will be reported to host upon changed: (97? command -1)
0  ready(to be connected)
1  connecting
2  connected

HFP: HFP status will be reported to host upon changed: (84? command -1)
0  ready(to be connected)
1  connecting
2  connected
3  call incoming
4  call outgoing 
5  call active

//ida
AVRCP Commands 
0x41 - vol up
0x42 - vol down
0x44 - play
0x45 - stop
0x46 - pause
0x4B - forward
0x4c - rewind

tx 3B 03 xx 00 00 //a2dp vol change
         FD //down
         FB //up
rx ?

tx 3D 02 0x 0x //avrcp
         08 fwd
         09 bcwd
rx ?

tx 58 01 0x //voice out switch
rx ?

tx 44 01 xx //sms download AT

tx 4a xx .. ...//send data to spp
      xx data length
         ..//data
rx ?

tx 57 01 xx //vr command, not in use
rx ?

tx 5B 60 ... //?

tx 60 01 53 //sms stop download

tx 62 02 01 0x//set 3g mode
            00 on
            01 off
rx ?